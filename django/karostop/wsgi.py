"""
WSGI config for karostop project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'karostop.settings')

application = get_wsgi_application()
#Ensure all the webcams are actually turned off at the start of the application
from .karoapi.models import Employee
Employee.objects.all().update(time_at_desk=0,second_counter=0,blink=0,on_break=False)
