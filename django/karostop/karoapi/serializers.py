from django.contrib.auth.models import User, Group
from rest_framework import serializers

from .models import Employee, Notification,FatigueRecord, Manager, BreakRequest
from django.contrib.auth import authenticate, get_user_model
from drf_writable_nested.serializers import WritableNestedModelSerializer

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id','username', 'first_name','last_name','email','is_staff']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ['id','sender','text','time','is_read']

class EmployeeSerializer(WritableNestedModelSerializer):
    user = UserSerializer(required=False)
    notifications = NotificationSerializer(many=True,required=False)
    class Meta:
        depth = 1
        model = Employee
        fields = ('id','user','notifications','time_of_last_break','fatigue_threshold','time_at_work','time_at_desk','on_break','blink','phone_number','dob','department','position')

class ManagerSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)
    notifications = NotificationSerializer(many=True,required=False)
    class Meta:
        model = Manager
        fields = ('id','user','notifications')

class BreakRequestSerializer(serializers.ModelSerializer):
    employee_first_name = serializers.ReadOnlyField(source="employee.user.first_name")
    employee_last_name = serializers.ReadOnlyField(source="employee.user.last_name")
    employee_last_break_time = serializers.ReadOnlyField(source="employee.lastBreakTime")
    class Meta:
        model = BreakRequest
        fields = ('id','employee','employee_first_name','employee_last_name','employee_last_break_time','time','is_accepted','is_denied')

class FatigueRecordSerializer(serializers.ModelSerializer):
    employee_id = serializers.ReadOnlyField(source="employee.id")
    employee_first_name = serializers.ReadOnlyField(source="employee.user.first_name")
    employee_last_name = serializers.ReadOnlyField(source="employee.user.last_name")
    employee_position = serializers.ReadOnlyField(source="employee.position")
    class Meta:
        model = FatigueRecord
        fields = ('id','time_of_record','fatigue_threshold','employee_id','employee_first_name','employee_last_name','employee_position')

#Custom JWT Serializer to allow log in with both email or username
from rest_framework_jwt.serializers import JSONWebTokenSerializer

from django.contrib.auth import authenticate, get_user_model
from django.utils.translation import ugettext as _
from rest_framework import serializers

from rest_framework_jwt.settings import api_settings
User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER

class CustomJWTSerializer(JSONWebTokenSerializer):
    username_field = 'username_or_email'

    def validate(self, attrs):

        password = attrs.get("password")
        user_obj = User.objects.filter(email=attrs.get("username_or_email")).first() or User.objects.filter(username=attrs.get("username_or_email")).first()
        if user_obj is not None:
            credentials = {
                'username':user_obj.username,
                'password': password
            }
            if all(credentials.values()):
                user = authenticate(**credentials)
                if user:
                    if not user.is_active:
                        msg = _('User account is disabled.')
                        raise serializers.ValidationError(msg)

                    payload = jwt_payload_handler(user)

                    return {
                        'token': jwt_encode_handler(payload),
                        'user': user
                    }
                else:
                    msg = _('Unable to log in with provided credentials.')
                    raise serializers.ValidationError(msg)

            else:
                msg = _('Must include "{username_field}" and "password".')
                msg = msg.format(username_field=self.username_field)
                raise serializers.ValidationError(msg)

        else:
            msg = _('Account with this email/username does not exists')
            raise serializers.ValidationError(msg)