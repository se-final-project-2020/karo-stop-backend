from django.contrib.auth.models import User,Group
from rest_framework import viewsets
from .serializers import UserSerializer, GroupSerializer, EmployeeSerializer, NotificationSerializer, ManagerSerializer,FatigueRecordSerializer,BreakRequestSerializer
from .models import Employee, Notification,Manager,FatigueRecord,BreakRequest
from django.shortcuts import render , get_object_or_404
from django.db.models import F,Q
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from django.http import HttpResponse
from rest_framework.decorators import api_view, permission_classes
from django.utils import timezone
import random
import datetime
import time
import json
import csv
from datetime import datetime,date,timedelta
from karostop.karoapi import controller
from django.core.mail import BadHeaderError, send_mail
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.contrib.auth.decorators import permission_required
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404
from django.core.validators import validate_email
from rest_framework import status, exceptions
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = get_user_model().objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class ManagerViewSet(viewsets.ModelViewSet):
    queryset = Manager.objects.all()
    serializer_class = ManagerSerializer

class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    
class FatigueRecordViewSet(viewsets.ModelViewSet):
    queryset = FatigueRecord.objects.all()
    serializer_class = FatigueRecordSerializer

class BreakRequestViewSet(viewsets.ModelViewSet):
    queryset = BreakRequest.objects.all()
    serializer_class = BreakRequestSerializer

@api_view(['GET'])
def connect(request,employeeId):
    #If the employee can't be found, throw 404 error
    if( not Employee.objects.filter(id=employeeId).exists() ):
        raise exceptions.NotFound({'employee_id':"Could not find any employee with the given employeeId"})
    else:
        #Update
        employee = Employee.objects.get(id=employeeId)
        #Calculate fatigueThreshold every minute
        if ( employee.second_counter >= 60):
            #Formula to calculate fatigueThreshold
            employee.fatigue_threshold = ( employee.blink * employee.fatigue_rate ) / 4
            employee.blink = 0
            employee.second_counter=0
            #Make a new FatigueRecord
            FatigueRecord.objects.create(time_of_record=timezone.now(),fatigue_threshold=employee.fatigue_threshold,employee_id=employeeId).save()    
        #Send a health notification concern if fatigueThreshold is 4 or more
            if ( employee.fatigue_threshold >= 4):
                newNotif =  Notification(sender="SYSTEM",
                    time=datetime.datetime.now(),
                    text="Recommend to take a break"
                )
                newNotif.save()
                Employee.objects.get(id=employeeId).notifications.add(newNotif) 
        #Update the data
        Employee.objects.filter(id=employeeId).update(
            fatigue_threshold= employee.fatigue_threshold,
            time_at_work = employee.time_at_work+1,
            time_at_desk = employee.time_at_desk,
            second_counter = employee.second_counter,
            blink = employee.blink
            )

        return JsonResponse(
                    {
                        'employee_id': employee.id,
                        'time_at_work': employee.time_at_work+1,
                        'time_at_desk' : employee.time_at_desk,
                        'fatigue_threshold' : employee.fatigue_threshold,
                        'on_break': employee.on_break,
                        'blink' : employee.blink
                    }
                ) 

# Toggle break for the employee
@api_view(['POST']) 
def take_break(request,employeeId):
    try:
        #If not on break yet, record the break time and set onBreak to true
        if (Employee.objects.get(id=employeeId).on_break == False):
            Employee.objects.filter(id=employeeId).update(time_of_last_break=timezone.now(),on_break=True)
            employee = Employee.objects.get(id=employeeId)
        else:
            Employee.objects.filter(id=employeeId).update(on_break=False)
            employee = Employee.objects.get(id=employeeId)
        return JsonResponse({
                "employee_id": employee.id,
                "time_of_last_break": employee.time_of_last_break,
                "on_break": employee.on_break
            })
    except ObjectDoesNotExist:
        raise exceptions.NotFound({'employee_id':"Could not find any employee with the given employeeId"})

# Mark a specific notification as read in the database
@api_view(['POST']) 
def mark_as_read(request,notificationId):
    if ( Notification.objects.filter(id=notificationId).exists() ):
        Notification.objects.filter(id=notificationId).update(is_read=True)
        notification = Notification.objects.get(id=notificationId) 
        return JsonResponse({
            "notification_id" : notification.id,
            "is_read" : notification.is_read
        })
    else:
        raise exceptions.NotFound({'notification_id':"Could not find any notification with the given notificationId"})

# Increment counter and time at desk if face is detected by webcam from the frontend
@api_view(['POST']) 
def update_time_at_desk(request,employeeId):
    try:
        Employee.objects.filter(id=employeeId).update(time_at_desk=F('time_at_desk')+1,second_counter=F('second_counter')+1)
        employee = Employee.objects.get(id=employeeId)
        return JsonResponse({
            "employee_id": employee.id,
            "time_at_desk": employee.time_at_desk
        })
    except ObjectDoesNotExist:
        raise exceptions.NotFound({'employee_id':"Could not find any employee with the given employeeId"})

# Get an employeeId which is linked to the given userId
@api_view(['GET']) 
def get_employee_id(request,userId):
    employee_id = get_employee_id_controller(userId)
    if (employee_id is None):
        raise exceptions.NotFound({'employee_id':"Could not find any employee with the given userId"})
    else:
        return JsonResponse({'employee_id':employee_id})
    #employee = None
    #try:
    #    employee = Employee.objects.get(user_id=userId)
    #except ObjectDoesNotExist:
    #    raise exceptions.NotFound({'employee_id':"Could not find any employee with the given userId"})
    #return JsonResponse({'employee_id':employee.id})

# Get a managerId which is linked to the given userId
@api_view(['GET'])
@staff_member_required
def get_manager_id(request,userId):
    manager_id = get_manager_id_controller(userId)
    if (manager_id is None):
        raise exceptions.NotFound({'manager_id':"Could not find any manager with the given userId"})
    else:
        return JsonResponse({'manager_id':manager_id})
    #manager = None
    #try:
    #    manager = Manager.objects.get(user_id=userId)
    #except ObjectDoesNotExist:
    #    raise exceptions.NotFound({'manager_id':"Could not find any manager with the given userId"})
    #return JsonResponse({'manager_id':manager.id})

#A combination of createNewEmployee and createNewManger with some error validations
#Might merge into one, depending on how the Aj see it
@api_view(['POST'])
@staff_member_required #Only staff is able to create a new user account, use Manager's JWT to process
def create_new_user_account(request):
    #Decode the content in request.body
    userContent = json.loads(request.body.decode('utf-8'))
    print(userContent)
    errorStack = {}
    newUser = None
    #Validate firstname
    if ('firstname' not in userContent):
        errorStack['firstname'] = "You must specify the first name"
    elif ( len(userContent['firstname']) < 2 ):
        errorStack['firstname'] = "First name must contain at least 2 characters"
    #Validate lastname
    if ('lastname' not in userContent):
        errorStack['lastname'] = "You must specify the last name"
    elif( len(userContent['lastname']) < 2 ):
        errorStack['lastname'] = "Last name must contain at least 2 characters"
    #Validate email
    if ('email' not in userContent):
        errorStack['email'] = "You must specify the email"
    else:
        try:
            validate_email(userContent['email'])
            if (get_user_model().objects.filter(email=userContent['email']).exists()):
                errorStack['email'] = "This email has already been used by the other user."
        except ValidationError as e:
            errorStack['email'] = e.messages
    #Validate username
    if ('username' not in userContent):
        errorStack['username'] = "You must specify the username"
    elif (len(userContent['username']) < 6):
        errorStack['username'] = "This username is too short. It must contain at least 6 characters."
    elif (get_user_model().objects.filter(username=userContent['username']).exists()):
        errorStack['username'] = "This username has already been used by the other user."
    #Validate password
    if ('password' not in userContent):
        errorStack['password'] = "You must specify the password"
    else:
        try:
            validate_password(userContent['password'])
        except ValidationError as e:
            errorStack['password'] = e.messages
    #If there are any errors, raise instead of creating a new user
    if errorStack:
        raise exceptions.ValidationError(errorStack)
    #Creat a new Employee or Manager, depending on the request
    if ( 'is_staff' not in userContent):
        userContent['is_staff'] = False
    #Create User
    newUser = get_user_model().objects.create_user(
        first_name=userContent['firstname'],
        last_name=userContent['lastname'],
        username=userContent['username'],
        email=userContent['email'], 
        password=userContent['password'],
        is_staff=userContent['is_staff']
    )
    #Create Manager if is_staff else create Employee
    if (userContent['is_staff'] == True):
        newManager = Manager.objects.create( user=newUser )
    else:
        newEmployee = Employee.objects.create( user=newUser )
    return JsonResponse({"status":"ok"})

############
#This method is an overriden method of the same name from 'django_rest_passwordreset'
#This method creates a password reset token and sends to the specified email
@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    """
    Handles password reset tokens
    When a token is created, an e-mail needs to be sent to the user
    :param sender: View Class that sent the signal
    :param instance: View Instance that sent the signal
    :param reset_password_token: Token Model Object
    :param args:
    :param kwargs:
    :return:
    """
    # send an e-mail to the user
    context = {
        'current_user': reset_password_token.user,
        'username': reset_password_token.user.username,
        'email': reset_password_token.user.email,
        #Original reset password url, left commented for reference
        #'reset_password_url_original': "{}?token={}".format(
            #instance.request.build_absolute_uri(reverse('password_reset:reset-password-confirm')),
            #reset_password_token.key),
        'reset_password_url': "{}?token={}".format(
            "http://127.0.0.1:4200/reset_password",
           
            reset_password_token.key)
            
    }

    
    # render email text
    #email_html_message = render_to_string('email/user_reset_password.html', context)
    email_plaintext_message = render_to_string('email/user_reset_password.txt', context)
    
    msg = EmailMultiAlternatives(
        # title:
        "Password Reset for {title}".format(title="Karō Stop"),
        # message:
        email_plaintext_message,
        # from:
        "noreply@somehost.local",
        # to:
        [reset_password_token.user.email]
    )
    #msg.attach_alternative(email_html_message, "text/html")
    msg.send()
############

#This method creates a csv contains fatigue records of all employees
@api_view(['POST'])
@staff_member_required #Only staff is able to create a new user account, use Manager's JWT to process
def create_fatigue_record_csv(request):
        response = HttpResponse(content_type='text/csv')
        body = {} if ( not request.body.decode('utf-8') ) else json.loads(request.body.decode('utf-8'))
        if ('this_week' not in body or (body['this_week'] == False)):
            body['this_week'] = False
        response = create_fatigue_record_csv_controller(body['this_week'])
        #if ('this_week' not in body or (body['this_week'] == False)):
        #    fatigueRecords = FatigueRecord.objects.all()
        #    response['Content-Disposition'] = 'attachment; filename="Fatigue Records.csv"'   
        #else:
        #    today =  ( date.today() )
        #    weekAgo =  ( today - timedelta(days=6) )
        #    fatigueRecords = FatigueRecord.objects.filter(time_of_record__range=[weekAgo,today])
        #    response['Content-Disposition'] = 'attachment; filename="Fatigue Records '+str(weekAgo)+'_'+str(today)+'.csv"'  
        #writer = csv.writer(response,csv.excel)
        #writer.writerow(['id','first_name','last_name','employee_id','position''fatigue_threshold','time_of_record'])  
        #for rec in fatigueRecords:
        #    first_name = rec.employee.user.first_name or ""
        #    last_name  = rec.employee.user.last_name  or ""
        #    emp_id  = rec.employee_id  or ""    
        #    position  = rec.employee.position  or ""   
        #    writer.writerow([
        #            rec.id,
        #            first_name,
        #            last_name,
        #            emp_id,
        #            position,
        #            rec.fatigue_threshold,
        #            rec.time_of_record
        #            ]
        #        )  
        return response

#Send a notification to all managers, all managers will receive the same notification with the same time
@api_view(['POST'])
def send_notification_to_managers(request):
    #Decode the content in request.body
    content = json.loads(request.body.decode('utf-8'))
    #Raise error if text is not present or empty
    if( 'text' not in content or len(content['text']) == 0 ):
        raise exceptions.ValidationError({'text':'Text for the notification can not be empty!'}, code='invalid')
    #Might be better to create new notification for each manager, depend on how it will work out
    newNotif =  Notification(sender="SYSTEM",
                    time=timezone.now(),
                    text=content['text']
                )
    newNotif.save()
    manager_id_list_queryset = Manager.objects.filter().values_list('id', flat=True)
    manager_id_list = list(manager_id_list_queryset)
    #Send notification to each manager
    for id in manager_id_list:
        Manager.objects.get(id=id).notifications.add(newNotif) 
    return JsonResponse( { "status":"notification sent to all managers" } )

#Likely a placeholder before we can figure out how to add notification from frontend without using a backend method
@api_view(['POST'])
def send_notification_to_employee(request):
    #Decode the content in request.body
    content = json.loads(request.body.decode('utf-8'))
    #Raise error if text is not present or empty
    errorStack = {}
    if( 'employee_id' not in content or content['employee_id'] is None):
        errorStack['employee_id'] = "You must specify an employee id!"
    elif not ( Employee.objects.filter(id=content['employee_id']).exists() ) :
        raise exceptions.NotFound({ "employee_id" : "This employee id does not exist!"})   
    if( 'text' not in content or len(content['text']) == 0 ):
        errorStack['text'] = "Text for the notification can not be empty!"
    #If there are any errors, raise instead of sending a notification 
    if errorStack:
        raise exceptions.ValidationError(errorStack)
    if (send_notification_to_employee_controller(content)):
    #Raise error if text is not present or empty
    #errorStack = {}
    #if( 'employee_id' not in content or content['employee_id'] is None):
    #    errorStack['employee_id'] = "You must specify an employee id!"
    #elif not ( Employee.objects.filter(id=content['employee_id']).exists() ) :
    #    raise exceptions.NotFound({ "employee_id" : "This employee id does not exist!"})   
    #if( 'text' not in content or len(content['text']) == 0 ):
    #    errorStack['text'] = "Text for the notification can not be empty!"
    #If there are any errors, raise instead of sending a notification 
    #if errorStack:
    #    raise exceptions.ValidationError(errorStack)
    #newNotif =  Notification(
    #                sender="SYSTEM",
    #                time=timezone.now(),
    #                text=content['text']
    #            )
    #newNotif.save()
    #Send notification to the employee
    #Employee.objects.get(id=content['employee_id']).notifications.add(newNotif) 
        return JsonResponse( { "status":"notification sent to employee id "+str(content['employee_id']) } )


def get_employee_id_controller(userId):
    if Employee.objects.filter(user_id=userId).exists():
        return Employee.objects.get(user_id=userId).id
    else:
        return None

def get_manager_id_controller(userId):
    if Manager.objects.filter(user_id=userId).exists():
        return Manager.objects.get(user_id=userId).id
    else:
        return None

def create_fatigue_record_csv_controller(this_week):
    response = HttpResponse(content_type='text/csv')
    fatigueRecords = None
    if (not this_week):
        fatigueRecords = FatigueRecord.objects.all()
        response['Content-Disposition'] = 'attachment; filename="Fatigue Records.csv"'   
    else:
        today =  ( date.today() )
        weekAgo =  ( today - timedelta(days=6) )
        fatigueRecords = FatigueRecord.objects.filter(time_of_record__range=[weekAgo,today])
        response['Content-Disposition'] = 'attachment; filename="Fatigue Records '+str(weekAgo)+'_'+str(today)+'.csv"'  
    writer = csv.writer(response,csv.excel)
    writer.writerow(['id','first_name','last_name','employee_id','position''fatigue_threshold','time_of_record'])  
    for rec in fatigueRecords:
        first_name = rec.employee.user.first_name or ""
        last_name  = rec.employee.user.last_name  or ""
        emp_id  = rec.employee_id  or ""    
        position  = rec.employee.position  or ""   
        writer.writerow([
                rec.id,
                first_name,
                last_name,
                emp_id,
                position,
                rec.fatigue_threshold,
                rec.time_of_record
                ]
            )  
    return response

def send_notification_to_employee_controller(content):
    newNotif =  Notification(
                    sender="SYSTEM",
                    time=timezone.now(),
                    text=content['text']
                )
    newNotif.save()
    #Send notification to the employee
    Employee.objects.get(id=content['employee_id']).notifications.add(newNotif) 
    return True
