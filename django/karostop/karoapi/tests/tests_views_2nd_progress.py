from django.test import TestCase,Client
from karostop.karoapi.serializers import GroupSerializer, EmployeeSerializer, NotificationSerializer
from karostop.karoapi.models import Employee, Notification, Manager, FatigueRecord,BreakRequest
import datetime
from karostop.karoapi import controller
from django.contrib.auth import get_user_model
import json
import csv
import io
import datetime
import pytz
from datetime import date,timedelta
from django.utils import timezone
from django.utils.timezone import make_aware
from karostop.karoapi import views
from rest_framework import status, exceptions
from django.core import management
from rest_framework.exceptions import ValidationError
from django.core.exceptions import  ObjectDoesNotExist
class GetEmployeeIdControllerTest(TestCase):
    #Set up once
    @classmethod
    def setUpTestData(cls):
        management.call_command('loaddata', 'fixture.json', verbosity=0)

    def test_get_employee_id_controller_success(self):
        result = views.get_employee_id_controller(userId=1)
        self.assertEqual(result,1)
        
      

    def test_get_employee_id_controller_fail(self):
        result = views.get_employee_id_controller(userId=999)
        self.assertEqual(result,None)

class GetManagerIdControllerTest(TestCase):
    #Set up once
    @classmethod
    def setUpTestData(cls):
        management.call_command('loaddata', 'fixture.json', verbosity=0)
        
    def test_get_manager_id_controller_success(self):
        result = views.get_manager_id_controller(userId=3)
        self.assertEqual(result,1)
      

    def test_get_manager_id_controller_fail(self):
        result = views.get_manager_id_controller(userId=999)
        self.assertEqual(result,None)

class SendNotificationToEmployeeControllerTest(TestCase):
    #Set up once
    @classmethod
    def setUpTestData(cls):
        management.call_command('loaddata', 'fixture.json', verbosity=0)
        cls.json_content_valid = json.loads('{ "text": "testing text", "employee_id" : 1 }')       

    def test_send_notification_to_employee_controller_success(self):
        result = views.send_notification_to_employee_controller(self.json_content_valid)
        self.assertEqual(result,True)
