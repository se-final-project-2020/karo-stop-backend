from django.db import models
import datetime

import time
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.
from django.contrib.auth import get_user_model
class Employee(models.Model):
    position = models.CharField(max_length=32)
    user = models.OneToOneField(get_user_model(),on_delete=models.CASCADE)
    notifications = models.ManyToManyField("Notification",null=True, default=None,related_name="employee_notification")
    time_at_work = models.IntegerField(default=0)
    time_at_desk = models.IntegerField(default=0)
    time_of_last_break = models.DateTimeField(auto_now_add=True)
    fatigue_threshold = models.FloatField(default=0.00)
    blink = models.IntegerField(default=0)
    second_counter = models.IntegerField(default=0)
    fatigue_rate = models.FloatField(default=1.00)
    on_break = models.BooleanField(default=False)

    dob = models.DateField(default=datetime.datetime(1999, 4, 22))
    phone_number = models.CharField(null=True,default="0804554263",max_length=12)
    department = models.CharField(null=True,default="Finance",max_length=40)

class Manager(models.Model):
    user = models.OneToOneField(get_user_model(),on_delete=models.CASCADE)
    notifications = models.ManyToManyField("Notification",null=True, default=None,related_name="manager_notification")

class Notification(models.Model):
    sender = models.CharField(max_length=32,default="SYSTEM")
    text = models.CharField(max_length=256)
    time = models.DateTimeField(default=timezone.now())
    is_read = models.BooleanField(default=False)

class BreakRequest(models.Model):
    employee = models.OneToOneField(Employee,on_delete=models.CASCADE,null=True)
    time = models.DateTimeField(default=timezone.now())
    is_accepted = models.BooleanField(default=False,null=True)
    is_denied = models.BooleanField(default=False,null=True)

class FatigueRecord(models.Model):
    employee = models.ForeignKey("Employee",null=True,on_delete=models.SET_NULL)
    time_of_record = models.DateTimeField(default=timezone.now())
    fatigue_threshold = models.FloatField(default=0.00)
    
