
from .models import Employee, Notification, Manager
from django.shortcuts import render
from django.db.models import F
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status, viewsets
from rest_framework.decorators import api_view
from django.utils import timezone
from karostop.karoapi import views
import random
import datetime
import time
from django.contrib.auth.models import User, Group
from .serializers import UserSerializer, GroupSerializer, EmployeeSerializer, NotificationSerializer
from django.contrib.auth import get_user_model
import json
import jwt
import uuid
import warnings
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from calendar import timegm
from datetime import datetime
from rest_framework import status, exceptions
from rest_framework_jwt.compat import get_username
from rest_framework_jwt.compat import get_username_field
from rest_framework_jwt.settings import api_settings

#This one is actually implemented by our JWT framework, we literally only added more field to the code
def jwt_payload_handler(user):
    username_field = get_username_field()
    username = get_username(user)
    
    warnings.warn(
        'The following fields will be removed in the future: '
        '`email` and `user_id`. ',
        DeprecationWarning
    )

    payload = {
        'user_id': user.pk,
        'username': username,
        'is_staff': user.is_staff,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }
    if hasattr(user, 'email'):
        payload['email'] = user.email
    if isinstance(user.pk, uuid.UUID):
        payload['user_id'] = str(user.pk)

    payload[username_field] = username

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload
