# Generated by Django 3.0.8 on 2020-10-14 15:35

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('karoapi', '0022_auto_20201014_2229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='breakrequest',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 14, 15, 35, 32, 842720, tzinfo=utc)),
        ),
    ]
