# Generated by Django 3.0.8 on 2020-10-03 16:33

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('karoapi', '0007_auto_20201003_2300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='breakrequest',
            name='isAnswered',
        ),
        migrations.AlterField(
            model_name='breakrequest',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2020, 10, 3, 16, 33, 5, 938524, tzinfo=utc)),
        ),
    ]
