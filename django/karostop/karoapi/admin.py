from django.contrib import admin
from .models import Employee

from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from rest_framework.decorators import api_view

# Register your models here.
admin.site.register(Employee)

