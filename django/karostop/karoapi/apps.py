from django.apps import AppConfig


class KaroapiConfig(AppConfig):
    name = 'karostop.karoapi'
