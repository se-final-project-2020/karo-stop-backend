from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from karostop.karoapi import views
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token,ObtainJSONWebToken
from karostop.karoapi.serializers import CustomJWTSerializer
from django.views.decorators.csrf import csrf_exempt

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'employees', views.EmployeeViewSet)
router.register(r'managers', views.ManagerViewSet)
router.register(r'notifications', views.NotificationViewSet)
router.register(r'fatigueRecords', views.FatigueRecordViewSet),
router.register(r'breakRequests', views.BreakRequestViewSet)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('karoapi/connect/<int:employeeId>', views.connect),
    path('karoapi/connectWebcam/<int:employeeId>', views.update_time_at_desk),
    path('takeBreak/<int:employeeId>',views.take_break),
    path('karoapi/markasread/<int:notificationId>',views.mark_as_read),
    path('karoapi/getFatigueRecords/',views.create_fatigue_record_csv),
    path('karoapi/sendEmployeeNotification/',views.send_notification_to_employee),
    path('karoapi/sendManagersNotification/',views.send_notification_to_managers),
   
    #Use custom JWT Login
    path('auth/login/', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer)),
    #path('auth/login/', obtain_jwt_token),
    path('auth/refresh-token/', refresh_jwt_token),

    path('karoapi/getEmployeeId/<int:userId>', views.get_employee_id),
    path('karoapi/getManagerId/<int:userId>', views.get_manager_id),

    #Use to reset password
    path('password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),

    #Put random stuff here
    path('test/createNewUserAccount/', views.create_new_user_account) #Might use this to replace createNewEmployee and createNewManager
    

]

